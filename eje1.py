#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import collections
import json


# cagar datos desde top50.csb
def carga_datos():
    carga = []
    datos = open("top50.csv")
    for x in datos:
        carga.append(x)
    return carga


# agrupar datos segun los tipos que haya
def agrupar(carga):
    datos = []
    for i in range(14):
        datos.append([])
        for j in range(len(carga)):
            x = carga[j].split(",")
            datos[i].append(x[i])
    return datos


# mediana de ruido
def ruido(ruido, ranking, cancion, artista, gen):
    ruido_ord = []
    # igualar una matriz
    for i in range(len(ruido)):
        ruido_ord.append(ruido[i])
    ruido_ord.sort()

    # calcular mediana que al ser 50 datos tomaria el 24 y 26 de indice
    mediana = (int(ruido_ord[24]) + int(ruido_ord[25]))/2
    # texto
    txt = "{0} | {1} | {2} | {3}"
    # recorrer y verificar si el ruido es igual a la mediana
    for i in range(1, len(ruido)):
        if int(ruido[i]) == mediana:
            print(txt.format(ranking[i], cancion[i], artista[i], gen[i]))


# encontrar generos mas bailables
def generos(generos, bailable):
    # variables
    bail = []
    menores = []
    mayores = []

    # igualar lsitas
    for i in range(len(bailable)):
        bail.append(bailable[i])
    bail.sort()

    # la lista ordenada del 0, 3 tendrá los 3 menos bailables
    for j in range(0, len(bailable)):
        for i in range(0, 3):
            if bail[i] == bailable[j]:
                menores.append(j)
    # lista del 47 al 49 tendrá los mas bailables
        for i in range(47, 49):
            if bail[i] == bailable[j]:
                if j in mayores:
                    pass
                else:
                    mayores.append(j)

    print("Los Generos menos bailables son\n")
    for i in range(len(menores)):
        print(generos[menores[i]])
    print("\n")
    print("Los generos mas bailables son\n")
    for i in range(len(mayores)):
        print(generos[mayores[i]])


def popular(popular, ruido, artista, cancion):
    popu = []
    ruid = []
    for i in range(0, len(popular)):
        popu.append(popular[i])
        ruid.append(ruido[i])

    popu.sort()
    ruid.sort()
    ultima = len(popu) - 2
    # encontrar posicion de la mas popular
    for j in range(1, len(popular)):
        for i in range(0, len(popu)):
            if popu[ultima] == popular[j]:
                pos = j

    # la menos ruidosa con la posicion de la mas popular
    if ruido[pos] == ruid[0]:
        print("", cancion[pos], artista[pos], popular[pos], ruido[pos])

    else:
        print("no")


def artistas(artistas):
    art_sin_rep = []
    repetidos = []

    print("Los artistas del top 50 son :")
    # for que recorre y agrega a una lista sin repetir artista
    for i in range(1, len(artistas)):
        print(artistas[i])
        if artistas[i] in art_sin_rep:
            repetidos.append(artistas[i])
        else:
            art_sin_rep.append(artistas[i])
    # print artistas
    print("\n")
    print("La cantidad de artistas son : ", len(art_sin_rep))

    # importar collections para saber cual es elemnto que mas se repite
    cuenta1 = collections.Counter(repetidos)
    x = cuenta1.most_common(1)

    enunciado = "El que mas se repite es {0} con {1} canciones"
    print(enunciado.format(x[0][0], (x[0][1]) + 1))


def crear_archi(top, can, art, gen, beat, leng, popu):
    dic = {}
    # eliminar el primer elemento que es una valor no deseado
    top.pop(0)
    can.pop(0)
    art.pop(0)
    gen.pop(0)
    beat.pop(0)
    leng.pop(0)
    popu.pop(0)

    # crear dic con la respectiva key y values
    dic["Ranking"] = top
    dic["Cancion"] = can
    dic["Artista"] = art
    dic["Genero"] = gen
    dic["Beat"] = beat
    dic["Length"] = leng
    dic["Popularity"] = popu

    # crear una variables datos para ingresarla al texto
    datos = json.dumps(dic)
    f = open('top50.json', 'w')
    f.write(datos)
    f.close
    # imprimir json
    for key, val in dic.items():
        print("{0}: {1}".format(key, val))


# para recortar la posicion de mi lista
"""
0.-"top"
1.-Track.Name"
2.-"Artist.Name"
3.-"Genre"
4.-"Beats.Per.Minute"
5.-"Energy"
6.-"Danceability"
7.-"Loudness..dB.."
8.-"Liveness"
9.-"Valence."
10.-"Length."
11.-"Acousticness.."
12.-"Speechiness."
13.-"Popularity"
    """


if __name__ == "__main__":
    # carga de datos
    carga = carga_datos()
    datos = agrupar(carga)
    # presentar opciones
    print("1.Listar artistas, cuantos artistas son y mas canciones")
    print("2.Mediana de ruido y cancion que entra en el rango de mediana")
    print("Listar Ranking, cancion, artista y genero")
    print("3.Los 3 generos mas bailables y mas lentas(tempo)")
    print("4.-cancion mas popular es la menos ruidosa?")
    print("5.Generar json")
    print("6.-Salir")
    # pedir opciones
    buscar = int(input("Que desea buscar: "))
    print("\n")

    # if's segun la opcion ingresada

    if buscar == 1:
        artistas(datos[2])

    elif buscar == 3:
        generos(datos[3], datos[6])

    elif buscar == 4:
        popular(datos[13], datos[7], datos[2], datos[1])

    elif buscar == 5:
        crear_archi(datos[0], datos[1], datos[2], datos[3], datos[4],
                    datos[10], datos[13])

    elif buscar == 2:
        ruido(datos[7], datos[0], datos[1], datos[2], datos[3])

    elif buscar == 6:
        print("ADIOS")
        exit()
    else:
        print("Error de datos, adios")


